# Copyright 2017-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

REVISION=""
BOOTSTRAP_VERSION="2.5.4"
BOOTSTRAP_ANT_VERSION="1.8.2"

SLOT="1.8"

require icedtea

PLATFORMS="~amd64 ~x86"

DOWNLOADS+=" https://www.bouncycastle.org/download/bcprov-jdk15on-161.jar"

# The vast majority of tests succeed but still too many fail. See src_test in
# icedtea.exlib for details.
RESTRICT="test"

# DISTRIBUTION_PATCHES as used in the Makefile is *NOT* an array so we can't
# magically make it one here. It only worked by fluke so far because there only
# ever was *one* patch - till now.
DISTRIBUTION_PATCHES="patches/exherbo.patch"

src_prepare() {
    # This check comes too early, kill it.
    edo sed -i -e '/^IT_FIND_TOOLS.*FASTJAR/d' configure.ac

    autotools_src_prepare

    edo cp "${FILES}/exherbo.patch" patches
}

src_configure() {
    # make icedtea build with gcc6+
    # see also https://patchwork.openembedded.org/patch/125471/
    # last checked: icedtea8-3.3.0
    append-flags "-fno-lifetime-dse -fno-delete-null-pointer-checks"
    append-cppflags "-std=gnu++98"
    icedtea_src_configure
}

src_install() {
    icedtea_src_install

    insinto /usr/$(exhost --target)/lib/${PN}/jre/lib/ext
    newins "${FETCHEDDIR}"/bcprov-jdk15on-161.jar bcprov.jar
}
