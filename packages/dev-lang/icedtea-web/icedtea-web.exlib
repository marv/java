# Copyright 2011-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic java [ virtual_jdk_and_jre_deps=false ]

export_exlib_phases src_configure src_install

SUMMARY="IcedTea-Web provides the IcedTea NP Plugin and Java Web Start"
DESCRIPTION="
IcedTea-Web is an addon component to IcedTea. It is open source (GPL + LGPL for
some parts) and provides a Java web browser plugin (IcedTea NP Plugin) and Java
Web Start implementation (i.e. javaws binary, based on NetX and referred to as such).
Additionally, it also contains itweb-settings, a GUI tool to control deployment
settings for NetX and the plugin."
BASE_URI="http://icedtea.classpath.org"
HOMEPAGE="${BASE_URI}/wiki/IcedTea-Web"
DOWNLOADS="${BASE_URI}/download/source/${PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    javadoc [[ description = [ Build tons of HTML API docs ] ]]
    nsplugin [[ description = [
                Build the browser plugin. Only enable it if you really need it as
                it's insecure.
                ]
             ]]
"

# "A test suite is supplied for the browser plugin. It can be built
# using 'make plugin-tests' and run by loading the HTML page specified
# into a browser with the plugin installed." - we can't do that.
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        virtual/jdk:=[>=1.7]
        x11-libs/libX11
        nsplugin?   (
            dev-libs/glib:2
            net-www/npapi-sdk
            x11-libs/gtk+:2
        )
"

icedtea-web_src_configure() {
    unset _JAVA_OPTIONS

    econf \
        $(option_enable javadoc docs) \
        $(option_enable nsplugin native-plugin) \
        --with-javac \
        --with-jdk-home=/usr/$(exhost --target)/lib/jdk \
        --with-pkgversion="Exherbo" \
        --without-ecj \
        --without-ecj-jar \
        --without-gcj \
        --without-rhino
}

icedtea-web_src_install() {
    default

    if option nsplugin ; then
        register_mozplugin /usr/$(exhost --target)/lib/IcedTeaPlugin.so
    else
        edo rmdir ${IMAGE}/usr/$(exhost --target)/lib
    fi
}

